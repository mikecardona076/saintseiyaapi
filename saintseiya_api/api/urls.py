from django.urls import path
from .views import *

app_name = 'API'
urlpatterns = [
    path('Athena/', AthenaViewSet.as_view(), name='AthenaViewSet'),
    path('AddSaint/', AthenaPostSaint.as_view(), name='AthenaPostSaint'),
]
