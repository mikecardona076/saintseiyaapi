from django.shortcuts import get_object_or_404
from  .serializer import *
from rest_framework.views import APIView
from rest_framework.response import Response
from sanctuary.models import *
from rest_framework import generics

# Create your views here.

class AthenaViewSet(APIView):
    def get(self, request, format=None):
        queryset = Saint.objects.all()
        serializer = AthenaSerializer(queryset, many=True)
        return Response(serializer.data)

class AthenaPostSaint(generics.CreateAPIView):
    serializer_class = AthenaSerializer
    queryset = Saint.objects.all()
    


