from rest_framework import serializers
from sanctuary.models import *

class AthenaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Saint
        fields = '__all__'
