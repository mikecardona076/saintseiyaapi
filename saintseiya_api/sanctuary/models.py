from django.db import models
from .Cloths import * 
# Create your models here.
class Saint(models.Model):
    name = models.CharField(max_length=20)
    cloth = models.CharField(max_length=30,choices=CLOTHS)
    date_birth = models.DateField()
    picture = models.ImageField(blank=True, upload_to='media')