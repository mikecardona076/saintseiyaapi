from django import forms
from .models import *

class SaintForm(forms.ModelForm):
    class Meta:
        model = Saint
        fields = '__all__'
        widgets = {
            'name' : forms.TextInput(attrs={'type' :"text", 'class' :"form-control", 'id' : "formGroupExampleInput", 'placeholder':"Name"}),
            'cloth' : forms.Select(),
            'date_birth' : forms.TextInput(attrs={'type' : 'date'}),
            'picture' : forms.FileInput(attrs={'type' : 'file', 'class' :"form-control",  'id' : "formFileMultiple"})
        }

  