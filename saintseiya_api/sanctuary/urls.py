from django.urls import path
from . import views
app_name = 'Athena'

urlpatterns = [
    path('', views.Index.as_view(), name='Index'),
    path('CreateAthenaSaint/', views.CreateAthenaSaint.as_view(), name='CreateAthenaSaint'),
    path('Apis/', views.All_Apis.as_view(), name='AllApis'),
    path('About/', views.About.as_view(), name='About'),
    path('Community/', views.Community.as_view(), name='help'),
]
