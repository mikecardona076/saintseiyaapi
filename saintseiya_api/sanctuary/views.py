from django.shortcuts import render
from django.views.generic import TemplateView, FormView, CreateView
from .forms import *
from django.urls import reverse_lazy

# Create your views here.

class Index(TemplateView):
    template_name = 'index.html'

class CreateAthenaSaint(CreateView):
    template_name = 'Form.html'
    form_class = SaintForm
    success_url = reverse_lazy('Athena:CreateAthenaSaint')

class All_Apis(TemplateView):
    template_name = 'apis.html'

class About(TemplateView):
    template_name = 'about.html'

class Community(TemplateView):
    template_name = 'community.html'


